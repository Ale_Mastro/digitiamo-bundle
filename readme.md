![alt text](./readme/banner.png)

## Table of Contents
- [Installation](#installation)
- [Components](#components-list)
  - [Alert Component](#alert-component)
  - [AutoComplete Component](#autocomplete-component)
  - [Chip Component](#chip-component)
  - [DataTable Component](#datatable-component)
  - [Header Component](#header-component)
  - [Login Component](#login-component)
  - [Pagination Component](#pagination-component)
  - [Sidebar Component](#sidebar-component)
  - [Skeleton Component](#skeleton-component)

## Installation:
```bash
npm install digitiamo-bundle
```

Add in the index.js this line:

```bash
import 'digitiamo-bundle/dist/style.css'
```

# Components List
## Alert Component:
The Alert component is a customizable alert box that can be used to display different types of messages to the user. It supports five types of alerts: "info", "highlight", "success", "warning", and "error". Each type of alert has a corresponding icon and CSS styling.

### Props
The Alert component accepts the following props:

- ***show***: A boolean that determines whether the alert box should be displayed.
- ***label***: An optional string that represents the message to be displayed in the alert box.
- ***type***: An optional string that represents the type of alert. It can be one of the following: "info", "highlight", "success", "warning", "error". The default value is "success".
- ***autoClose***: An optional boolean that determines whether the alert box should automatically close after a certain period of time. The default value is false.
- ***time***: An optional number that represents the amount of time (in milliseconds) after which the alert box should automatically close. The default value is 3000.
### Usage
Here's an example of how to use the Alert component:
```javascript
import { Alert } from './Alert';

function App() {
  return (
    <div>
      <Alert show={true} label="This is an info alert" type="info" autoClose={true} time={5000} />
    </div>
  );
}

export default App;
```
In this example, an "info" alert with the message "This is an info alert" is displayed. The alert box will automatically close after 5 seconds.

## AutoComplete Component:
The AutoComplete component is a customizable input field with autocomplete functionality. It allows users to quickly find and select from a pre-populated list of options as they type, leveraging searching and filtering.

### Props
The AutoComplete component accepts the following props:

- ***placeholder***: An optional string that specifies the placeholder text to be displayed in the input field.
- ***theme***: An optional object that allows you to customize the main color of the component for light and dark themes.
- ***value***: A string or undefined value that represents the current value of the autocomplete input field.
- ***setValue***: A function that is used to update the value of the autocomplete component.
- ***options***: An array of objects that represent the available options for the autocomplete component. Each object in the array has two properties: label and value.
### Usage
Here's an example of how to use the AutoComplete component:
```javascript
import { AutoComplete } from './AutoComplete';

function App() {
  const [value, setValue] = useState("");
  const options = [
    { label: "Option 1", value: "1" },
    { label: "Option 2", value: "2" },
    // Add more options as needed
  ];

  return (
    <div>
      <AutoComplete value={value} setValue={setValue} options={options} placeholder="Select an option" />
    </div>
  );
}

export default App;
```
In this example, an AutoComplete component with a placeholder "Select an option" is displayed. As the user types in the input field, the options that match the user's input are displayed in a dropdown list.

## Chip Component:
The Chip component is a small, interactive element that can contain a main label. It is used to represent a complex entity in a small block, such as a contact. It supports five types of chips: "info", "highlight", "success", "warning", and "error". Each type of chip has a corresponding CSS styling.

### Props
The Chip component accepts the following props:

- ***label***: A string that represents the main label of the chip.
- ***type***: A string that represents the type of chip. It can be one of the following: "info", "highlight", "success", "warning", "error". The default value is "info".
### Usage
Here's an example of how to use the Chip component:
```javascript
import { Chip } from './Chip';

function App() {
  return (
    <div>
      <Chip label="This is an info chip" type="info" />
    </div>
  );
}

export default App;
```
In this example, an "info" chip with the label "This is an info chip" is displayed.


## DataTable Component:
The DataTable component is a customizable data table for your React application. It provides a flexible way to display a collection of data with optional search and sorting functionalities.

### Props
The DataTable component accepts the following props:

- ***values***: An array of objects that define the data to be displayed in the table.

- ***columns***: An array of Column objects that define the columns of the table. Each Column object can have the following properties:

  - ***title**: The display text of the column header.
  - ***render**: A function that defines how the data for this column should be rendered.
- ***orderBy***: A function that defines how the data should be sorted when a column header is clicked.

- ***search***: An optional boolean that determines whether the search functionality should be enabled. Default is false.

### Usage
Here is a basic usage example of the DataTable component:
```javascript
import { DataTable } from './DataTable';

const App = () => {
  const data = [
    { name: 'John', age: 30 },
    { name: 'Jane', age: 25 },
    { name: 'Doe', age: 35 },
  ];

  const columns = [
    { title: 'Name', render: (row) => row.name },
    { title: 'Age', render: (row) => row.age },
  ];

  const orderBy = (title, direction) => {
    // Implement sorting functionality here
  };

  return (
    <DataTable values={data} columns={columns} orderBy={orderBy} search={true} />
  );
};

export default App;
```
In this example, the DataTable component displays a data table with two columns: Name and Age. The orderBy function is called when a column header is clicked to sort the data. The search functionality is enabled with the search prop.



## Header Component:
The Header component is a customizable header for your React application. It provides a flexible way to display a list of items with optional child items.

### Props
The Header component accepts the following props:

- ***height***: An optional string that defines the height of the header. Default is "50px".

- ***positioning***: An optional string that defines the positioning of the header content. Default is "end".

- ***gap***: An optional number that defines the gap between the header items. Default is 3.

- ***colors***: An optional object that defines the colors of the header. It can include the following properties:

  - ***bgHeader***: The background color of the header.
  - ***opacity***: The opacity of the header.
  - ***border***: The color of the border of the header.
  - ***textColor***: The color of the text in the header.
- ***content***: An array of child objects that define the items in the header. Each child object can have the following properties:

  - ***title***: The display text of the item.
  - ***list***: An optional boolean that determines whether the item has a sublist.
  - ***render***: An optional custom render function for the item.
  - ***children***: An optional array of child objects for the sublist items.

### Usage
Here is a basic usage example of the Header component:
```javascript
import { Header } from './Header';

const App = () => {
  const content = [
    { title: 'Home' },
    { title: 'About' },
    { title: 'Contact' },
  ];

  return (
    <Header content={content} />
  );
};

export default App;
```
In this example, the Header component displays a header with three items: Home, About, and Contact.


## Login Component:
The Login component is a customizable login form for your React application. It provides a flexible way to handle user authentication with optional social login buttons.

### Props
The Login component accepts the following props:

- ***background***: An optional object that defines the background of the login form. It includes an img property for the background image, a src property for the image source, and an opacity property for the image opacity.

- ***colors***: An optional Colors object that defines the colors of the login form. It can include the following properties:

  - ***boxBorder***: The color of the border of the login box.
  - ***input***: The color of the input fields.
  - ***hr***: The color of the horizontal rule.
  - ***buttons***: The color of the buttons.
- ***content***: An optional object that defines the dimensions of the left and right sections of the login form.

- ***errorMsg***: An optional string that defines the error message to display when the login fails.

- ***height*** and ***width***: Optional strings that define the height and width of the login form.

- ***img***: An optional image to display in the login form.

- ***placeholder***: An optional object that defines the placeholder text for the username and password input fields.

- ***showError***: An optional boolean that determines whether to show the error message.

- ***socialButtons***: An optional array of social login buttons to display.

- ***subtitle***: An optional string that defines the subtitle of the login form.

- ***theme***: An optional string that defines the theme of the login form. It can be either "light" or "dark".

- ***title***: A string that defines the title of the login form.

- ***loginFunction***: A function that handles the login process when the login button is clicked.

### Usage
Here is a basic usage example of the Login component:
```javascript
import { Login } from './Login';

const App = () => {
  const handleLogin = () => {
    // Handle the login process here
  };

  return (
    <Login
      title="Login"
      placeholder={{ username: "Username", password: "Password" }}
      loginFunction={handleLogin}
    />
  );
};

export default App;
```
In this example, the Login component displays a login form with a title and placeholders for the username and password input fields. The handleLogin function is called when the login button is clicked.



## Pagination Component:
The Pagination component is a reusable component in TypeScript React that renders a pagination UI with previous and next buttons, as well as numbered buttons for navigating through pages.

### Props
- ***number***: The total number of pages in the pagination.
- ***onChange***: A function that is called when the page number changes. It receives the new page number as its argument.

### Usage
```javascript
import { Pagination } from './component/Pagination';

<Pagination
  number={totalPages}
  onChange={(pageNumber) => {
    // handle page change
  }}
/>
```
In the above example, replace totalPages with the total number of pages that you want to display in the pagination. The onChange prop is a function that will be called whenever the page number changes. The new page number will be passed as an argument to this function.

## Sidebar Component:
The **Sidebar** component is a responsive sidebar navigation component for your React application. It provides a flexible way to display a collection of items with optional nested navigation items.

### Props
The Sidebar component accepts the following props:

- ***header***: An optional object that defines the header of the sidebar. It includes an img property for the header image and a text property for the header text.

- ***content***: An array of ListElement objects that define the navigation items in the sidebar. Each ListElement object can have the following properties:

  - ***title***: The display text of the navigation item.
  - ***icon***: An optional icon for the navigation item.
  - ***render***: An optional custom render function for the navigation item.
  - ***url***: The URL that the navigation item links to.
  - ***children***: An optional array of Child objects for nested navigation items.
- ***colors***: An optional Colors object that defines the colors of the sidebar. It can include the following properties:

  - ***sidebarBg***: The background color of the sidebar.
  - ***elementBg***: The background color of a navigation item.
  - ***hoverBrightness***: The brightness level when a navigation item is hovered.
  - ***headerText***: The color of the header text.
elementText: The color of a navigation item text.

### Usage
Here is a basic usage example of the Sidebar component:
```javascript
import { Sidebar } from './Sidebar';

const App = () => {
  const header = { img: 'header.jpg', text: 'Header Text' };
  const content = [
    { title: 'Home', url: '/' },
    { title: 'About', url: '/about' },
    { title: 'Contact', url: '/contact' },
  ];
  const colors = { sidebarBg: '#2f3f51', elementBg: '#46aa8d', hoverBrightness: 10, headerText: '#ffffff', elementText: '#ffffff' };

  return (
    <Sidebar header={header} content={content} colors={colors} />
  );
};
export default App;
```
In this example, the Sidebar component displays a sidebar with a header and three navigation items. The colors of the sidebar are customized with the colors prop.

## Skeleton Component:

The Skeleton component is a reusable React component that renders a rectangular or rounded skeleton element with customizable width, height, and class name. It's typically used as a placeholder while content is being loaded.

### Props
The Skeleton component accepts the following props:

- ***type**: Specifies the type of skeleton to render. It can be either "rect" or "rounded". The default value is "rect".
- ***width**: An optional string that specifies the width of the skeleton. The default value is "100%".
- ***height**: An optional string that specifies the height of the skeleton. The default value is "20px".
- ***className**: An optional string that can be used to apply additional CSS classes to the skeleton.
### Usage
Here's an example of how to use the Skeleton component:
```javascript
import { Skeleton } from './Skeleton';

function App() {
  return (
    <div>
      <Skeleton type="rounded" width="50px" height="50px" className="my-skeleton" />
    </div>
  );
}

export default App;
```
In this example, a rounded skeleton with a width and height of 50px is displayed. The "my-skeleton" class is applied to the skeleton.