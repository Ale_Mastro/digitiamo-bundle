import {Alert} from '../component/Alert'; 

export default {
  title: 'Alert',
  component: Alert,
};

export const Default = {
args: {
    type: 'info',
    message: 'This is an informational alert',
},
};


export const Success = {
args: {
    ...Default.args,
    type: 'success',
    message: 'This is a success alert',
},
};

export const Warning = {
args: {
    ...Default.args,
    type: 'warning',
    message: 'This is a warning alert',
},
};

// Example of an "error" alert
export const Error = {
args: {
    ...Default.args,
    type: 'error',
    message: 'This is an error alert',
},
};