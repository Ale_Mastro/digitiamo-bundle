import React from 'react';
import { StoryFn} from '@storybook/react';
import  { Colors } from '../component/Sidebar'
import ToggleButton from '../component/ToggleButton';

export default {
  title: 'ToggleButton',
  component: ToggleButton,
} 

const Template: StoryFn<{ isSidebarNotOpen: boolean; colors: Colors }> = (args) => (
  <ToggleButton {...args} />
);


export const Default = Template.bind({});
Default.args = {
  isSidebarNotOpen: true,
  colors: {
    sidebarBg: '#2f3f51',
    elementBg: '#46aa8d',
    hoverBrightness: 10,
    headerText: '#ffffff',
    elementText: '#ffffff',
  },
};


