import { useState } from 'react';
import { AutoComplete,AutoCompleteProps } from '../component/AutoComplete';
import { StoryFn } from '@storybook/react';

export default {
  component: AutoComplete,
  title: 'AutoComplete',
 
};



const Template = (args: AutoCompleteProps) => {
  
  const [value, setValue] = useState('');
  
 
  
  return <AutoComplete value={value} setValue={setValue} placeholder={args.placeholder} options={args.options} />;
};




export const Default: StoryFn<AutoCompleteProps> = Template.bind({
 
  
});

Default.args = {
  options: [
    { label: 'Option 1', value: '1' },
    { label: 'Option 2', value: '2' },
    { label: 'Option 3', value: '3' },
  
  ],
  placeholder:'boooo',
}


