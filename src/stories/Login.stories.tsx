import { StoryFn } from "@storybook/react";
import { Login, LoginProps } from "src/component/Login";

export default {
    title:'Login',
    component:Login
}

const Template = (args:LoginProps) => {
    
  
    return <Login {...args}/>;
};

export const Default: StoryFn<LoginProps> = Template.bind({})

Default.args = {
    socialButtons: [
   
    ],
    height: '100%',
    width: '100%',
    errorMsg: 'Something went wrong, please check your credentials',
    content: {
      left: { dimension: '100%' },
      right: { dimension: '100%' },
    },
    background: { img: false, src: '', opacity: 1 },
    theme: 'light',
    colors: {
      boxBorder: {
        dark: 'border-[#e6e7e7] rounded-lg border',
        light: 'border-[#e6e7e7] rounded-lg border',
      },
      input: {
        dark: 'bg-gray-700 border-gray-600 placeholder-gray-400 text-white focus:ring-blue-500 focus:border-blue-500',
        light: 'bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-50',
      },
      hr: {
        dark: ' bg-gray-700',
        light: 'bg-gray-200',
      },
      buttons: {
        dark: 'h-[40px] w-full border rounded mt-2',
        light: 'h-[40px] w-full border rounded mt-2',
      },
    },
    img: (
      <>
        <img src="./bg_bggenerator_com.png" className="w-full h-full object-cover" />
        <img src="./digitiamoLogo.png" className="w-[30%] object-cover absolute h-[30%]" style={{ filter: 'brightness(100)' }} />
      </>
    ),
    placeholder: {
      username: 'Username',
      password: 'Password',
    },
    showError: false,
    title: 'Login',
    subtitle: 'Welcome in Digitiamo',
    loginFunction: () => {
      // Your login function logic here
    },
  };