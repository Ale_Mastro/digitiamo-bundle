
import { DataTable } from '../component/DataTable';

export default {
  title: 'DataTable',
  component: DataTable,
};


export const Default = {
  args: {
    values: [
      // Add sample data for testing
      // Example: { id: 1, name: 'Matt', age: 25, ... }
    ],
    columns: [
      // Add sample columns for testing
      // Example: { title: 'ID', render: (row) => row.id },
    ],
    orderBy: (title: string, direction: string) => {
    
      console.log(`Order by ${title} in ${direction} direction`);
    },
    search: true,
  },
};


