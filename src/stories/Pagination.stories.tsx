import { StoryFn } from '@storybook/react';

import { Pagination, PaginationProps } from '../component/Pagination';

export default {
  title: 'Pagination',
  component: Pagination,
} 

const Template: StoryFn<PaginationProps> = (args) => <Pagination {...args} />;


export const Default = Template.bind({});
Default.args = {
  number: 10,
  onChange: (pageNumber: number) => {
    // Your onChange function logic here
    console.log(`Page changed to: ${pageNumber}`);
  },
};


