
import { Chip} from '../component/Chip';

export default {
  title: 'Chip',
  component: Chip,
}


export const Default = {
  args: {
    label: 'Default Chip',
    type: 'info',
  },
};


export const Highlight = {
  args: {
    label: 'Highlight Chip',
    type: 'highlight',
  },
};


export const Success = {
  args: {
    label: 'Success Chip',
    type: 'success',
  },
};

export const Warning = {
  args: {
    label: 'Warning Chip',
    type: 'warning',
  },
};

export const Error = {
  args: {
    label: 'Error Chip',
    type: 'error',
  },
};


