import { StoryFn } from '@storybook/react';

import { Skeleton, SkeletonProps } from '../component/Skeleton';

export default {
  title: 'Skeleton',
  component: Skeleton,
}

const Template: StoryFn<SkeletonProps> = (args) => <Skeleton {...args} />;


export const Default = Template.bind({});
Default.args = {
  type: 'rect',
  width: '100%',
  height: '20px',
  className: 'your-custom-class',
};


export const Rounded = Template.bind({});
Rounded.args = {
  type: 'rounded',
  width: '50px',
  height: '50px',
  className: 'your-custom-class',
};


