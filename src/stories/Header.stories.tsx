import { Header } from '../component/Header';

export default {
  title: 'Header',
  component: Header,
  argTypes: {
    content: { control: { disable: true } }, 
  },
};


export const Default = {
  args: {
    height: '50px',
    positioning: 'end',
    gap: 3,
    colors: {
      bgHeader: '#2f3f51',
      opacity: '100',
      border: '#46aa8d',
      textColor: '#fff',
    },
    content: [
      { title: 'Home' },
      {
        title: 'Services',
        list: true,
        children: [
          { title: 'Service 1' },
          { title: 'Service 2' },
          { title: 'Service 3' },
        ],
      },
      { title: 'Contact' },
    ],
  },
};


