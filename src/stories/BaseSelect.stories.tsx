import React, { useState } from 'react';
import BaseSelect, { BaseSelectProps } from '../component/BaseSelect';
import {StoryFn} from '@storybook/react'

export default {
  title: 'BaseSelect',
  component: BaseSelect,

};

const Template = (args:BaseSelectProps) => {
  const [value, setValue] = useState('');

  return <BaseSelect {...args} value={value} onChange={setValue} />;
};


export const Default: StoryFn<BaseSelectProps> = Template.bind({});

Default.args = {
    title: 'Your Title Here',
    small: 'Subtitle Here dsfsdxzzczvxcxvxcvfgdsfgsdfgvds',
    options: [
      { label: 'Option 1', value: '1' },
      { label: 'Option 2', value: '2' },
      { label: 'Option 3', value: '3' },
   
    ],
    value: '1',
    colLeft: '4',
    colRight: '8',
    className: '',
}