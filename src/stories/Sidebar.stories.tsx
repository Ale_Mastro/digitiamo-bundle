import { StoryFn } from '@storybook/react';

import { Sidebar, SidebarProps } from '../component/Sidebar';

export default {
  title: 'Sidebar',
  component: Sidebar,
}

const Template: StoryFn<SidebarProps> = (args) => <Sidebar {...args} />;


export const Default = Template.bind({});
Default.args = {
  header: {
    img: 'path/to/your/image.jpg',
    text: 'Your Header Text',
  },
  content: [
    {
      title: 'Home',
    
      url: '/home',
      children: [
        {
          title: 'Subitem 1',
          url: '/subitem1',
        },
        {
          title: 'Subitem 2',
          url: '/subitem2',
        },
      ],
    },
    {
      title: 'About',
    
      url: '/about',
    },
 
  ],
  colors: {
    sidebarBg: '#2f3f51',
    elementBg: '#46aa8d',
    hoverBrightness: 10,
    headerText: '#ffffff',
    elementText: '#ffffff',
  },
};


