import React from 'react';
import { StoryFn } from '@storybook/react';

import SocialButton, { SocialButtonProps } from '../component/SocialButton';

export default {
  title: 'SocialButton',
  component: SocialButton,
}

const Template: StoryFn<SocialButtonProps> = (args) => <SocialButton {...args} />;


export const Default = Template.bind({});
Default.args = {
  label: 'Facebook',
  icon: '',
  onClick: () => console.log('Clicked Facebook'),
  className: 'your-custom-class',
};


