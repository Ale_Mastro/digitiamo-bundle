import React, { useState } from 'react';
import {BaseInputText, BaseInputTextProps} from '../component/BaseInputText';
import { StoryFn } from '@storybook/react';

export default {
  title: 'BaseInputText',
  component: BaseInputText,
 
};

const Template = (args:BaseInputTextProps) => {
  const [value, setValue] = useState('');
  args = {
    ...args,
    value:value,
    onChange:setValue
  };

  return <BaseInputText {...args} />;
};


export const Default: StoryFn<BaseInputTextProps> = Template.bind({
 
});

Default.args = {
  title: 'Your Title Here',
  subtitle: 'Your Subtitle Here',
  type: 'text',
  placeholder: 'Type something...',
  className: '',
  colLeft: '4',
  colRight: '8',
  value:'',
  onChange:Function
}


// You can create more stories to represent different states or variations
