import React from "react";
/* The `interface BaseSelectProps` is defining the props that can be passed to the `BaseSelect`
component. It specifies that the component expects a `title` prop of type `string`, an `options`
prop which is an array of objects with `label` and `value` properties of type `string` or `Number`,
a `value` prop of any type, and an `onChange` prop of type `Function`. These props are used within
the component to render a select input with the specified options and to handle changes to the
selected value. */
export interface BaseSelectProps {
  title?: string;
  small?: string;
  options: { label: string; value: string | number }[] | null;
  value: string;
  // eslint-disable-next-line @typescript-eslint/ban-types
  onChange: Function;
  colLeft?: string;
  colRight?: string;
  className?:string;
}
/**
 * This is a TypeScript React component that renders a select element with options and a title, and
 * calls a callback function with the selected value on change.
 */
export default function BaseSelect({
  title,
  small,
  options,
  value,
  onChange,
  colLeft='4',
  colRight='8',
  className=""
}: BaseSelectProps) {

/**
 * The handleChange function is used to handle the change event of a select element in a TypeScript
 * React component.
 * @param e - The parameter `e` is of type `React.ChangeEvent<HTMLSelectElement>`. This means it is an
 * event object that is triggered when the value of a `<select>` element is changed.
 */
  const handleChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    onChange(e.target.value);
  };
  return (
    <div className="form-group flex">
      {title && (
        <div className={`w-${colLeft}/12 flex justify-start items-center`}>
          <label>{title}{small &&  <small>{" "}{small}</small>}</label>
        </div>
      )}
      <div className={title ? `w-${colRight}/12` : "w-full"}>
        <select className={`${className} form-control  w-full`} onChange={handleChange} defaultValue={value}>
          {options &&
            options.map(
              ({
                label,
                value: val,
              }: {
                label: string;
                value: string | number;
              }) => (
                // @ts-ignore
                <option value={val} key={val}>
                  {label}
                </option>
              )
            )}
        </select>
      </div>
    </div>
  );
}
