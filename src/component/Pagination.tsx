import { useEffect, useState } from 'react';
export interface PaginationProps {
    number: number;
    onChange: (pageNumber: number) => any
}
/**
 * The `Pagination` component is a reusable component in TypeScript React that renders a pagination UI
 * with previous and next buttons, as well as numbered buttons for navigating through pages.
 * @param {PaginationProps}  - - `number`: The total number of pages in the pagination.
 */
export const Pagination = ({ number, onChange } : PaginationProps) => {
    const [start, setStart] = useState(1);
    const cssPoint = "px-2 mt-[5px] h-[25px] min-w-[25px] flex justify-center items-center border-[1px] text-xs";
    const cssButton = `${cssPoint} cursor-pointer hover:!bg-[#1ab394] hover:!text-white`;
    useEffect(() => onChange(start), [start])
    /**
     * The function `renderButton` returns a div element with a dynamic class name and onClick event
     * handler.
     * @param {number} value - The `value` parameter is a number that represents the value of the
     * button.
     */
    const renderButton = (value: number) => (
        <div
            key={value}
            className={`${cssButton} ${start === value && 'bg-[#1ab394] !text-white'}`}
            onClick={() => setStart(value)}
        >
            {value}
        </div>
    );

    /**
     * The renderPoint function returns a div element with a CSS class name of cssPoint.
     */
    const renderPoint = () => <div className={cssPoint}>...</div>;

    /**
     * The function `renderButtons` generates an array of buttons with a specified length and starting
     * value.
     * @param {number} length - The `length` parameter represents the number of buttons that need to be
     * rendered.
     * @param {number} startValue - The startValue parameter is the initial value for the buttons. It
     * is the value that will be assigned to the first button.
     */
    const renderButtons = (length: number, startValue: number) => [...Array(length)].map((_, i) => renderButton(startValue + i));

    /* The `showPreviousButton` function is a helper function that conditionally renders a "Previous"
    button. It checks if the `start` value is greater than 1, and if so, it returns a `<div>`
    element with the class name `cssButton` and an `onClick` event handler that updates the `start`
    state by subtracting 1. The button displays the text "Previous". If the `start` value is not
    greater than 1, the function returns `null`, meaning no button will be rendered. */
    const showPreviousButton = () =>
        start > 1 &&
            <div className={` ${cssButton}`}
            onClick={() => setStart(start - 1)}
            >
                Previous
            </div>;

    /* The `showNextButton` function is a helper function that conditionally renders a "Next" button.
    It checks if the `start` value is less than the `number` prop, and if so, it returns a `<div>`
    element with the class name `cssButton` and an `onClick` event handler that updates the `start`
    state by adding 1. The button displays the text "Next". If the `start` value is not less than
    the `number` prop, the function returns `null`, meaning no button will be rendered. */
    const showNextButton = () =>
        start < number &&
            <div className={` ${cssButton}`}
            onClick={() => setStart(start + 1)}
            >
                Next
            </div>;

    return (
        <div className='w-full'>
            <div className="flex items-center justify-center">
            {number <= 10
                ?
                    renderButtons(number, 1)
                    :
                        <>
                        {showPreviousButton()}
                        {start > 4 && renderButton(1)}
                        {start > 4 && start < number - 3 && renderPoint()}
                        {start <= 4 ? renderButtons(5, 1) : (start > 4 && start < number - 3 && renderButtons(3, start - 1))}
                        {start > 4 && start < number - 3 && renderPoint()}
                        {start >= number - 3 && renderPoint()}
                        {start >= number - 3 ? renderButtons(3, number - 3) : (start <= 4 && renderPoint())}
                        {renderButton(number)}
                        {showNextButton()}
                        </>
            }
            </div>
        </div>
    )
}