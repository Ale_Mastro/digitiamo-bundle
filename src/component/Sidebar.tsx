import { useState, useEffect } from 'react';
import ToggleButton from './ToggleButton';


interface Child {
  title: string;
  url: string;
  render?: Function;
  icon?: any | null;
}
interface ListElement {
  title: string;
  icon?: any | null;
  render?: Function;
  url: string;
  children?: Child[];
}
interface SidebarHeader { img: string, text?: string }
export interface Colors { sidebarBg?: string, elementBg?: string, hoverBrightness?: number, headerText?: string, elementText?: string }
export interface SidebarProps {
  header?: SidebarHeader | null;
  content: ListElement[],
  colors?: Colors
}
const defaultColors = { sidebarBg: "#2f3f51", elementBg: "#46aa8d", hoverBrightness: 10, headerText: "#ffffff", elementText: "#ffffff" }

export const Sidebar = ({ header = null, content, colors = defaultColors }: SidebarProps) => {
  const [isSidebarNotOpen, setSidebarOpen] = useState(true);
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  const [hoveredParent, setHoveredParent] = useState(-1);
  const [selectedParent, setSelectedParent] = useState(0);
  const [selectedChild, setSelectedChild] = useState(0);


  /**
   * The function `renderDefault` returns either an icon or the first letter of the title in uppercase
   * if the sidebar is not open, otherwise it returns the result of calling the `expandedElement`
   * function with the title and icon.
   * @param {string} title - A string representing the title of the element.
   * @param {any} icon - The `icon` parameter is of type `any`, which means it can accept any data
   * type. It is used to specify an icon that will be displayed in the rendered output.
   * @returns The function `renderDefault` returns either the `icon` if it exists and the sidebar is
   * not open, or it returns the result of calling the `expandedElement` function with the `title` and
   * `icon` as arguments.
   */
  const renderDefault = (title: string, icon: any) => {
    return isSidebarNotOpen ? icon ? icon : title[0].toUpperCase() : expandedElement({ title, icon })
  }

  /**
   * The function `expandedElement` takes an object with a `title` and `icon` property, and returns a
   * React component that displays the title and icon in a flex container.
   * @param element - The `element` parameter is an object with two properties:
   * @param [className] - The `className` parameter is a string that represents the CSS classes to be
   * applied to the `div` element. It is optional and defaults to an empty string if not provided.
   */
  const expandedElement = (element: { title: string, icon: any }, className = "") => (<div className={`${className} whitespace-nowrap hover:text-white flex justify-start gap-3 w-full items-center`}><p className='w-5'>{element.hasOwnProperty('icon') ? element.icon : ""}</p><p>{element.title}</p></div>)

  /**
   * The function toggleSidebar toggles the state of the sidebar between open and closed.
   */
  const toggleSidebar = () => {
    setSidebarOpen(!isSidebarNotOpen);
  };

  /**
   * The function `handleResize` updates the state variable `windowWidth` with the current inner width
   * of the window.
   */
  const handleResize = () => {
    setWindowWidth(window.innerWidth);
  };

  useEffect(() => {
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  useEffect(() => {
    if (windowWidth >= 768) {
      setSidebarOpen(true);
    } else {
      setSidebarOpen(true);
    }
  }, [windowWidth]);

  return (
    <>
      <div className={`h-full w-[60px]`} style={{ fontFamily: `"open sans", "Helvetica Neue", Helvetica, Arial, sans-serif` }}>
        <div className={`fixed z-30 transition-all bottom-0 duration-300 cursor-pointer ${!isSidebarNotOpen ? "left-[250px]" : "left-[60px]"}`} onClick={toggleSidebar}>
          <ToggleButton isSidebarNotOpen={isSidebarNotOpen} colors={colors} />
        </div>
        <div className={`z-50 fixed top-0 bottom-0 transition-all duration-300 ease-in-out ${!isSidebarNotOpen ? 'w-[250px]' : 'w-[60px]'}`} style={{ backgroundColor: colors.sidebarBg }} >
          {/* User or Logo */}
          {header && <div className='p-3'>
            <img className={`rounded-full m-auto h-[${isSidebarNotOpen ? 30 : 130}px]`} src={header.img} />
            {header.text && !isSidebarNotOpen && <p style={{ color: colors.headerText }} className={`w-full text-center mt-2 text-sm`}>{header.text}</p>}
          </div>}
          <div className='mt-5'>
            {content.map((element, index: number) => (
              <div
                key={`${element.title}_${index}`}
                className={`relative text-[13px] font-semibold cursor-pointer px-3 py-3 ${isSidebarNotOpen ? 'flex justify-center items-center  px-4 py-2' : 'px-7'}`}
                style={{
                  color: colors.elementText,
                  background: isSidebarNotOpen ?
                    index === hoveredParent && index !== selectedParent ?
                      `${colors.elementBg}${colors.hoverBrightness}`
                      : selectedParent === index ? `linear-gradient(to right, ${colors.elementBg} 0%,${colors.elementBg} 7%, ${colors.elementBg}${colors.hoverBrightness} 7%)` : colors.sidebarBg
                    : index === hoveredParent && index !== selectedParent ?
                      `${colors.elementBg}${colors.hoverBrightness}` :
                      selectedParent === index ? `linear-gradient(to right, ${colors.elementBg} 0%,${colors.elementBg} 2%, ${colors.elementBg}${colors.hoverBrightness} 2%)` : colors.sidebarBg

                }}
                onMouseEnter={() => setHoveredParent(index)}
                onMouseMove={() => setHoveredParent(index)}
                onMouseLeave={() => setHoveredParent(-1)}
                onClick={() => setSelectedParent(index)}
              >
                {/* Main Parent */}
                <div >{renderDefault(element.title, element.icon)}</div>
                {/* Children */}
                {/* Case sidebar closed */}
                {isSidebarNotOpen && index === hoveredParent &&
                  <div style={{ background: colors.sidebarBg }} className={`absolute top-0 left-[60px] w-fit`}>
                    <div
                      onMouseEnter={() => setHoveredParent(index)}
                      onMouseLeave={() => setHoveredParent(-1)}
                      onClick={() => setSelectedParent(index)}
                      style={{ backgroundColor: `${colors.elementBg}${colors.hoverBrightness}` }}>
                      {element.children && element.children.map((child: Child, childIndex: number) => (
                        <div key={`${child.title}_${childIndex}`} className="inline-block px-4 py-2 whitespace-nowrap hover:text-white">{child.title}</div>
                      ))}
                    </div>
                  </div>
                }
                {/* Case sidebar open */}
                {!isSidebarNotOpen &&
                  <>
                    {element.children && element.children.map((child: Child, childIndex: number) => (
                      <div
                        onMouseEnter={() => setHoveredParent(index)}
                        onMouseLeave={() => setHoveredParent(-1)}
                        onClick={() => { setSelectedParent(index); setSelectedChild(childIndex) }}
                      >
                        <div key={`${child.title}_${childIndex}`}>
                          {expandedElement({ title: child.title, icon: child.icon }, `whitespace-nowrap pt-3 pl-3 text-[13px]  ${selectedParent === index && selectedChild === childIndex ? '' : 'opacity-80'}`)}
                        </div>
                      </div>
                    ))}
                  </>
                }
              </div>
            ))}
          </div>
        </div>
      </div>
    </>
  );
};