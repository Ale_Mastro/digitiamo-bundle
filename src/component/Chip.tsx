import React from 'react'
export interface ChipProp { 
    label: string;
    type: "info" | "highlight" | "success" | "warning" | "error"
}
export const Chip = ({ label, type = "info" } : ChipProp) => {
  const classes = {
    info: "text-white bg-[#306efd]",
    highlight: "text-gray bg-[#56cbef]",
    success: "text-white bg-[#368754]",
    warning: "text-gray bg-[#f6c00a]",
    error: "text-white bg-[#dc3546]"
  }
  return (
    <div className={`border w-fit px-3 text-sm rounded-md ${classes[type]}`} >{ label }</div>
  )
}
