
/* The `interface BaseInputTextProps` defines the props that can be passed to the `BaseInputText`
component. */
export interface BaseInputTextProps {
  value: any;
  onChange: Function;
  title?: string;
  subtitle?: string;
  type?: string;
  placeholder?: string;
  className?: string;
  colLeft?: string;
  colRight?: string;
}

/**
 * The `BaseInputText` function is a TypeScript React component that renders an input field with a
 * label, subtitle, and optional placeholder.
 * @param {BaseInputTextProps}  - - `value`: The current value of the input field.
 * @returns a JSX element that represents a form input field. The input field has various props such as
 * value, onChange, title, subtitle, type, placeholder, className, colLeft, and colRight. The value
 * prop is used to set the initial value of the input field, the onChange prop is a function that will
 * be called when the value of the input field changes, the title prop
 */
export function BaseInputText({ value, onChange, title, subtitle, type = "text", placeholder = "", className="", colLeft="4", colRight="8"  }: BaseInputTextProps) {
/**
 * The function `updateValue` is a TypeScript function that takes in an event of type
 * `React.ChangeEvent<HTMLInputElement>` and calls the `onChange` function with the value of the input
 * element.
 * @param event - The event parameter is of type React.ChangeEvent<HTMLInputElement>. This is a generic
 * type that represents an event that occurs when the value of an input element changes. It provides
 * access to the target property, which refers to the input element that triggered the event.
 */
  const updateValue = (event : React.ChangeEvent<HTMLInputElement>) => {
    onChange(event.target.value)
  }
  return (
    <div className={`form-group flex ${className}`}>
      {title && <div className={`w-${colLeft}/12 flex justify-start items-center`}>
        <label>
          {title}
          {subtitle && (
            <>
              <br />
              <small>{subtitle}</small>
            </>
          )}
        </label>
      </div>}
      <div className={title ? `w-${colRight}/12` : "w-full"}>
        <input
          value={value}
          onChange={updateValue}
          type={type}
          id="inputfield"
          className="bg-white border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary focus:border-primary block w-full p-2.5"
          placeholder={placeholder}
          required
        />
      </div>
    </div>
  );
}
