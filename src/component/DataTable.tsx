import { useEffect, useState } from "react";
import BaseSelect from "./BaseSelect";
import Skeleton from "react-loading-skeleton";
import {BaseInputText} from "./BaseInputText";
import "./BaseCustomDataTable.css"
import { FaSortUp } from 'react-icons/fa'
import { FaSortDown } from 'react-icons/fa'
import { BiRefresh } from 'react-icons/bi'
interface Column {
  title: string;
  render: Function;
}
export interface BaseTableProps {
  values: any[];
  columns: Column[];
  orderBy: Function;
  search?: boolean;
  // refresh?: boolean;
  // refreshFunction?: Function;
}

export const DataTable = ({
  values,
  columns,
  orderBy,
  search = false,
  // refresh = false,
  // refreshFunction = (_: any) => { }
}: BaseTableProps) => {
  const [data, setData] = useState(null)
  const [fullData, setFullData] = useState(null)
  const [order, setOrder] = useState({ value: "", order: "ASC" });
  const [start, setStart] = useState(1);
  const [length, setLength] = useState(10);
  const [totalRecords, setTotalRecords] = useState(0);
  const [totalRecordsFiltered, setTotalRecordsFiltered] = useState(0);
  const [searchText, setSearchText] = useState("");


  const searchFunction = (value: string) => {
    //@ts-ignore
    const filtered = fullData.filter((row) => columns.some(({ render }) => render(row, false, true)?.toString().includes(value) || false));
    setData(filtered)
    setTotalRecords(filtered.length);
    setTotalRecordsFiltered(filtered.length);
    // tableComponent.current.updateData(filtered);

  }

  const orderData = (title: string) => {
    let direction = "ASC";
    if (title === order.value) {
      direction = order.order === "ASC" ? "DESC" : "ASC";
    }
    setOrder({ value: title, order: direction });
    orderBy(title, direction);
  };

  useEffect(() => {
    //@ts-ignore
    setData(values);
    //@ts-ignore
    setFullData(values);
    setTotalRecords(values.length);
    setTotalRecordsFiltered(values.length);
  }, [values])

  return (
    <>
      <div>
        <div className="w-full flex items-center justify-between sticky left-0">
          <div className="w-1/2 flex">

            {/* {refresh && <div className="w-1/12">
            <BiRefresh size='1.5rem' onClick={() => refreshFunction()} />
          </div>} */}
            <div className="w-11/12">
              <BaseSelect
                value={length}
                title="Show entries:"
                options={[
                  { label: "10", value: 10 },
                  { label: "25", value: 25 },
                  { label: "50", value: 50 },
                  { label: "100", value: 100 },
                ]}
                onChange={(value: number) => { setLength(value); setStart(1); }}
              />
            </div>
          </div>
          {search && <div className="w-1/2">
            <BaseInputText
              title="Search:"
              value={searchText}
              onChange={(value: string) => { setSearchText(value); searchFunction(value) }}
            />
          </div>}
        </div>
        <div className="w-full overflow-auto relative max-h-[500px] mt-3">
          <table className="w-full">
            <thead>
              <tr className="w-full">
                {columns.map(({ title }) => (
                  <th
                    key={title}
                    className="px-3 cursor-pointer border-[#dcdcdc66] bg-white border-[2px] border-l-[0px] first:border-l-[2px] p-2 sticky top-[-1px]"
                    onClick={() => orderData(title)}
                  >
                    <div className="flex justify-between items-center">
                      <p>{title}</p>
                      <div className="flex flex-col gap-0">
                        <FaSortUp size='0.7rem' color={title === order.value && order.order === "ASC"
                          ? "#4ab494"
                          : "#dcdcdc"} />
                        <FaSortDown size='0.7rem' color={title === order.value && order.order === "DESC"
                          ? "#4ab494"
                          : "#dcdcdc"} />
                        <i
                          className={`fa fa-sort-desc !text-[${title === order.value && order.order === "DESC"
                            ? "#4ab494"
                            : "#dcdcdc"
                            }]`}
                        />
                      </div>
                    </div>
                  </th>
                ))}
              </tr>
            </thead>
            <tbody>
              {data && (<>{data
    //@ts-ignore
                .slice(
                  (start - 1) * length,
                  // @ts-ignore
                  (start - 1) * length + parseInt(length)
                )
    //@ts-ignore

                .map((row, index) => (
                  <tr
                    key={index}
                    className="w-full break-words border-collapse odd:bg-[#e6f6f28c] hover:bg-[#e0e0e05e]"
                  >
                    {columns.map(({ render }, indexCol) => (
                      <td
                        key={`${index}_col_${indexCol}`}
                        className="max-w-full px-3 break-words border-[#dcdcdc66] border-[2px] border-l-[0px] first:border-l-[2px] p-2 whitespace-nowrap overflow-hidden text-ellipsis"
                      >
                        {render(row)}
                      </td>
                    ))}
                  </tr>
                ))}</>)}
              {!data &&
                [...Array(length).keys()].map((index) => (
                  <tr
                    key={index}
                    className=" break-words border-collapse odd:bg-[#e6f6f28c] hover:bg-[#e0e0e05e]"
                  >
                    {columns.map((_, indexCol) => (
                      <td
                        key={`${index}_col_${indexCol}`}
                        className="px-3 border-[#dcdcdc66] border-[2px] border-l-[0px] first:border-l-[2px] p-2 whitespace-nowrap overflow-hidden text-ellipsis"
                      >
                        <Skeleton />
                      </td>
                    ))}
                  </tr>
                ))}
            </tbody>
          </table>
        </div>

        {Math.ceil(totalRecordsFiltered / length) <= 5 && (
          <div className="w-full flex sticky left-0">
            {" "}
            {Array.from(
              { length: Math.ceil(totalRecordsFiltered / length) },
              (_, i) => 1 + i
            ).map((c) => (
              <div
                key={c}
                className={`custom-data-table-button ${start === c ? "selected" : ""
                  }`}
                onClick={() => setStart(c)}
              >
                {c}
              </div>
            ))}
          </div>
        )}

        {Math.ceil(totalRecordsFiltered / length) > 5 && (
          <div className="w-full flex sticky left-0 mt-3">
            <div className="w-1/2">
              Showing {(start - 1) * length + 1} to{" "}
              {(start - 1) * length + length > totalRecordsFiltered
                ? totalRecordsFiltered
                : (start - 1) * length + length}{" "}
              of {totalRecords} entries
            </div>
            <div className="w-1/2">
              <div className="flex justify-end">
                {start > 1 && (
                  <div
                    className="px-2 custom-data-table-button"
                    onClick={() => setStart(start - 1)}
                  >
                    Previous
                  </div>
                )}
                {start > 4 && (
                  <div
                    className={`custom-data-table-button ${start === 1 ? "selected" : ""
                      }`}
                    onClick={() => setStart(1)}
                  >
                    {1}
                  </div>
                )}
                {start > 4 &&
                  start < Math.ceil(totalRecordsFiltered / length) - 3 && (
                    <div className="custom-data-table-point">...</div>
                  )}
                {start <= 4 &&
                  Array.from({ length: 5 - 1 + 1 }, (_, i) => 1 + i).map((c) => (
                    <div
                      key={c}
                      className={`custom-data-table-button ${start === c ? "selected" : ""
                        }`}
                      onClick={() => setStart(c)}
                    >
                      {c}
                    </div>
                  ))}

                {start > 4 &&
                  start < Math.ceil(totalRecordsFiltered / length) - 3 &&
                  Array.from({ length: 3 }, (_, i) => start - 1 + i).map((c) => (
                    <div
                      key={c}
                      className={`custom-data-table-button ${start === c ? "selected" : ""
                        }`}
                      onClick={() => setStart(c)}
                    >
                      {c}
                    </div>
                  ))}
                {start > 4 &&
                  start < Math.ceil(totalRecordsFiltered / length) - 3 && (
                    <div className="custom-data-table-point">...</div>
                  )}

                {start >= Math.ceil(totalRecordsFiltered / length) - 3 &&
                  Array.from(
                    { length: 4 },
                    (_, i) => Math.ceil(totalRecordsFiltered / length) - 4 + i
                  ).map((c) => (
                    <div
                      key={c}
                      className={`custom-data-table-button ${start === c ? "selected" : ""
                        }`}
                      onClick={() => setStart(c)}
                    >
                      {c}
                    </div>
                  ))}
                {start <= 4 && <div className="custom-data-table-point">...</div>}

                <div
                  className={`custom-data-table-button ${start === Math.ceil(totalRecordsFiltered / length)
                    ? "selected"
                    : ""
                    }`}
                  onClick={() =>
                    setStart(Math.ceil(totalRecordsFiltered / length))
                  }
                >
                  {Math.ceil(totalRecordsFiltered / length)}
                </div>
                {start < Math.ceil(totalRecordsFiltered / length) && (
                  <div
                    className="px-2 custom-data-table-button"
                    onClick={() => setStart(start + 1)}
                  >
                    Next
                  </div>
                )}
              </div>
            </div>
          </div>
        )}

      </div>
    </>
  );
}
