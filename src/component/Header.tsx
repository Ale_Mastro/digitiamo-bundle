import React, { useEffect, useState } from 'react'
interface child {
    title: string,
    list?: boolean,
    render?: Function,
    children?:
    { title: string, render?: Function }[]
}
export interface HeaderProps {
    height?: string;
    positioning?: string;
    gap?: number;
    colors?: { bgHeader: string, opacity: string | number, border: string, textColor: string };
    content: child[];
}
const defaultColors = { bgHeader: "#2f3f51", opacity: "100", border: "#46aa8d", textColor: "#fff" };

export const Header = ({ height = "50px", positioning = "end", gap = 3, colors = defaultColors, content = [] }: HeaderProps) => {
    /** Index of the opened list */
    const [listViewed, setListViewed] = useState(-1);

    /**
     * The function "hideList" sets the value of "listViewed" to -1. This means that the content of the sub list is hide
     */
    const hideList = () => {
        setListViewed(-1)
    }

    /**
     * The function "showList" sets the "listViewed" state to the given index and prevents event
     * propagation.
     * @param e - The "e" parameter is an event object that represents the event that triggered the
     * function. It is commonly used in event handlers to access information about the event, such as
     * the target element or the event type. In this case, it is used to stop the event from
     * propagating further, meaning that
     * @param index - The index parameter represents the index of the item in the list that is being
     * clicked or selected.
     */
    const showList = (e: React.MouseEvent<HTMLDivElement>, index: number) => {
        e.stopPropagation();
        setListViewed(index)
    }

    useEffect(() => {
        /* The line `window.addEventListener('click', hideList);` is adding an event listener to the window
        object. It listens for a click event anywhere on the window and calls the `hideList` function when
        the click event occurs. This allows the content of the sub list to be hidden when the user clicks
        outside of it. */
        window.addEventListener('click', hideList);

        return () => {
            /* The line `window.removeEventListener('click', hideList);` is removing the event listener
            that was added to the window object in the `useEffect` hook. This is done to clean up
            the event listener when the component is unmounted or when the dependencies of the
            `useEffect` hook change. By removing the event listener, we ensure that the `hideList`
            function is no longer called when a click event occurs outside of the component. */
            window.removeEventListener('click', hideList);
        };
    }, []);
    return (
        <div style={{ height, fontFamily: `"open sans", "Helvetica Neue", Helvetica, Arial, sans-serif`, borderColor: colors.border }} className={`fixed left-0 right-0 top-0 flex justify-${positioning} items-center px-5 gap-${gap} border-b-[1px]`}>
            <div className={`absolute top-0 left-0 right-0 bottom-0 z-[200] opacity-${colors.opacity}`} style={{ background: colors.bgHeader }}></div>
            {content.map((element, index) => (
                <div className='font-semibold font-sans z-[201]' key={`${element.title}_${index}`} style={{ color: colors.textColor }}>
                    {element.list
                        ? <div className='relative h-full' onClick={(e) => showList(e, listViewed === index ? -1 : index)}>
                            <div className='flex justify-center items-center gap-2 cursor-pointer'>
                                <p>{element.title}</p>
                                <div className='font-sm text-xs transition-all' style={{ rotate: listViewed === index ? "0deg" : "180deg" }}>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="6" height="6" viewBox="0 0 6 6" fill={colors.textColor}><path d="M6 5.5h-6l3-5z" /></svg></div>
                            </div>

                            <div className='absolute overflow-clip right-0 my-3 border-[1px]' style={{ borderColor: colors.border, height: index === listViewed ? "" : "0" }}>
                                {index === listViewed && <div className='relative w-full h-full'>
                                    <div className={`absolute top-0 left-0 right-0 bottom-0 z-[-100] opacity-${colors.opacity}`} style={{ background: colors.bgHeader }}></div>
                                    <div className='py-3 px-9'>
                                        {element.children && element.children.map((child) => <div key={`${child.title}_${index}`} className='whitespace-nowrap cursor-pointer'>{child.title}</div>)}
                                    </div>
                                </div>}
                            </div>

                        </div>
                        : <div className='relative cursor-pointer'>{element.render ? element.render() : element.title}</div>}
                </div>
            ))}
        </div>
    )
}
