/**
 * The SocialButton component is a reusable button component in TypeScript React that displays a label
 * and an icon, and triggers an onClick function when clicked.
 * @param  - - `label`: The text label for the social button.
 */

// eslint-disable-next-line @typescript-eslint/ban-types
export interface SocialButtonProps { label: string, icon: any, onClick: Function, className: string }

export default function SocialButton({ label, icon, onClick, className }: SocialButtonProps) {
    return (
        <div className={`flex justify-center items-center cursor-pointer ${className}`} onClick={(e) => onClick()}>
            <div className='flex items-center justify-between w-auto h-full px-3'>
                {icon}
            </div>
            <div className='w-full text-center'>{label}</div>
        </div>)
}
