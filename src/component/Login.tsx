import { error } from 'console'
import React, { useState } from 'react'
import SocialButton from './SocialButton'
import { BiShow } from 'react-icons/bi'
const colorsDefault = {
    boxBorder: {
        dark: "border-[#e6e7e7] rounded-lg border",
        light: "border-[#e6e7e7] rounded-lg border"
    },
    input: {
        dark: `bg-gray-700 border-gray-600 placeholder-gray-400 text-white focus:ring-blue-500 focus:border-blue-500`,
        light: `bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-50`
    },
    hr: {
        dark: " bg-gray-700",
        light: "bg-gray-200"
    },
    buttons: {
        dark: "h-[40px] w-full border rounded mt-2",
        light: "h-[40px] w-full border rounded mt-2"
    }

};
interface Color {
    dark: string,
    light: string
};
type ColorVariant = Color | string;

interface Colors {
    boxBorder: ColorVariant,
    input: ColorVariant,
    hr: ColorVariant,
    buttons: ColorVariant;
    [key: string]: ColorVariant;
}

export interface LoginProps {
    socialButtons?: any[],
    height?: string,
    width?: string,
    errorMsg?: string,
    content?: {
        left: { dimension: string },
        right: { dimension: string }
    },
    background?: { img: boolean, src: string, opacity: number },
    theme?: "light" | "dark",
    colors?: Colors,
    img?: any,
    showError?: boolean,
    title: string,
    subtitle?: string,
    placeholder?: {
        username: string,
        password: string
    },
    loginFunction: Function
}

export const Login = ({
    background = { img: false, src: "", opacity: 1 },
    colors = colorsDefault,
    content = {
        left: { dimension: "100%" },
        right: { dimension: "100%" }
    },
    errorMsg = "Something went wrong, please check your credentials",
    height = "100%",
    img = (
        <>
            <img src="./bg_bggenerator_com.png" className='w-full h-full object-cover' />
            <img src="./digitiamoLogo.png" className='w-[30%]  object-cover absolute h-[30%]' style={{ filter: "brightness(100)" }} />
        </>
    ),
    placeholder = {
        username: "Username",
        password: "Password"
    },
    showError = false,
    socialButtons = [],
    subtitle = "Welcome in Digitiamo",
    theme = "light",
    title = "Login",
    width = "100%",
    loginFunction

}: LoginProps) => {
    const [pwdHover, setPwdHover] = useState(false)
    /**
     * The function `getColors` returns a merged object of default colors and custom colors.
     * @returns The function `getColors` is returning an object that is a combination of the
     * `colorsDefault` object and the `colors` object.
     */
    const getColors = (): Colors => {
        return { ...colorsDefault, ...colors }
    }
    /**
     * The function `getClass` returns the value of a specific key from an object, based on the current
     * theme.
     * @param {string} key - The `key` parameter is a string that represents the key of an object
     * property.
     * @returns The function `getClass` is returning the value of `getColors()[key][theme]` if
     * `getColors()[key]` has a property named `theme`. Otherwise, it returns the value of
     * `getColors()[key]`.
     */
    const getClass = (key: string) => {
        // @ts-ignore
        return getColors()[key].hasOwnProperty(theme) ? getColors()[key][theme] : getColors()[key];
    }
    return (
        <div style={{ width, height }}
            className={`${getClass("boxBorder")} overflow-auto`}>
            <div
                className='flex w-full h-full relative'>
                {background.img && <div className='absolute h-full w-full bg-left-top bg-no-repeat' style={{ opacity: background.opacity, backgroundSize: "100% 100%", backgroundImage: background.img ? `url(${background.src})` : "" }}></div>}
                {!background.img && <div className={`w-0`}>
                    <div className=' h-full flex-col flex justify-center items-center relative'>{img}</div>
                </div>}
                <div className={`px-4 z-10`} style={{width:content.right.dimension}}>
                    <div className='h-full flex-col flex justify-center items-center'>
                        <div className='font-bold text-3xl'>{title}</div>
                        <div>{subtitle}</div>
                        <div className='w-full'>
                            <input
                                type="text"
                                className={`${getClass("input")} my-3 text-sm rounded-lg block w-full p-2.5`}
                                placeholder={placeholder.username}
                                required
                            />
                            <div className='relative bg-transparent'>
                                <input
                                    type={pwdHover ? "text" : "password"}
                                    className={`${getClass("input")} mt-3 text-sm rounded-lg block w-full p-2.5`}
                                    placeholder={placeholder.password} required
                                />
                                <BiShow
                                    size='1.5rem'
                                    color='#2f3f51'
                                    className='absolute top-[50%] translate-y-[-50%] right-4'
                                    onMouseLeave={() => setPwdHover(false)}
                                    onMouseEnter={() => setPwdHover(true)}
                                />
                            </div>
                        </div>
                        {showError && <small className='p-2 text-red-500'>{errorMsg}</small>}
                        <div className='w-full border rounded-md p-2 flex justify-center items-center cursor-pointer mt-3' onClick={() => loginFunction()}>
                            Log in
                        </div>
                        <div className={`${getClass("hr")} w-full h-px my-8 border-b-[1px]`} />
                        {socialButtons.map(
                            (element, index) => <SocialButton {...element} className={getClass("buttons")} key={index} />
                        )}
                    </div>
                </div>
            </div>
        </div>
    )
}
