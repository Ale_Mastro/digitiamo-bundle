import React from 'react'
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

export const Linechart = ({ 
    data,
    legend = true,
    tooltip = true,
    strokeDasharray = "3 3",
    x = {
        label: "Name",
        key: "name",
        type:"category",
        angle: 0,
        tickCount: 10,
        hide: false,
        width: 1,
        height:1
    },
    series = [
        { key: "pv", color: "#8884d8", type: "monotone", activeDot: {r : 8} },
        { key: "uv", color: "#82ca9d", type: "monotone", activeDot: {r : 8} },
    ]
 }) => {

    return (
        <ResponsiveContainer width="100%" height="100%">
            <LineChart
                data={data}
                //   margin={{
                //     top: 5,
                //     right: 30,
                //     left: 20,
                //     bottom: 5,
            //   }}
            >
                <CartesianGrid strokeDasharray={strokeDasharray}/>
                <XAxis angle={x.angle} tickCount={x.tickCount} dataKey={x.key} hide={x.hide} width={x.width} height={x.height} type={x.type}/>
                <YAxis />
                {tooltip && <Tooltip />}
                {legend && <Legend />}
                {series.map(({ key, color, type, activeDot}) => (
                    // @ts-ignore
                    <Line type={type} dataKey={key} stroke={color} activeDot={activeDot} />
                ))}
            </LineChart>
        </ResponsiveContainer>
    )
}

