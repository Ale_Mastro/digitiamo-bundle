export interface SkeletonProps {
    type?: "rect" | "rounded" | null | undefined;
    width?: string;
    height?: string;
    className?: string;
}
/**
 * The Skeleton component is a reusable React component that renders a rectangular or rounded skeleton
 * element with customizable width, height, and class name.
 * @param {SkeletonProps}  - - `type`: Specifies the type of skeleton to render. It can be either
 * "rect" or "rounded". The default value is "rect".
 */
export const Skeleton = ({ type = "rect", width = "100%", height = "20px", className = "" } : SkeletonProps) => {
    return (
        <>
            <div className={`skeleton className`}
                style={{
                    aspectRatio: type === 'rounded' ? "1/1" : "",
                    borderRadius: type === 'rounded' ? "50%" : "5px",
                    height: type === 'rounded' ? "" : height,
                    width,
                }}>
            </div>
        </>
    )
}
