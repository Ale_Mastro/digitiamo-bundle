import React, { ReactElement, useEffect, useState } from 'react'
import { AiOutlineClose, AiOutlineWarning } from "react-icons/ai"
import { BiInfoCircle } from "react-icons/bi"
import { CiCircleCheck } from "react-icons/ci"

/* The `const classes` is an object that maps different types of alerts to their corresponding CSS
classes. Each key in the object represents a type of alert (e.g., "info", "highlight", "success",
"warning", "error"), and the value is the CSS class string associated with that type of alert. These
CSS classes define the border color, text color, and background color for each type of alert. */
const classes: { [key: string]: string } = {
    info: "border-[#184461] text-[#184461] bg-[#8db0be]",
    highlight: "border-[#184461] text-[#184461] bg-[#e5f6fd]",
    success: "border-[#3e6244] text-[#3e6244] bg-[#edf7ed]",
    warning: "border-[#663c01] text-[#663c01] bg-[#fdf4e5]",
    error: "border-[#764141] text-[#764141] bg-[#fceded]"
};

/* The `iconLookup` constant is an object that maps different types of alerts to their corresponding
icons. Each key in the object represents a type of alert (e.g., "info", "highlight", "error",
"warning", "success"), and the value is the React element representing the icon associated with that
type of alert. */
const iconLookup: { [key: string]: ReactElement } = {
    "info": <BiInfoCircle size='1.5rem' />,
    "highlight": <BiInfoCircle size='1.5rem' />,
    "error": <BiInfoCircle size='1.5rem' />,
    "warning": <AiOutlineWarning size='1.5rem' />,
    "success": <CiCircleCheck size='1.5rem' />
};

export type AlertProps = {
    show: boolean,
    label?: string,
    type?: "info" | "highlight" | "success" | "warning" | "error",
    autoClose?: boolean,
    time?: number
}

/* The code defines a functional component called `Alert` that takes in several props: `show`, `type`,
`label`, `autoClose`, and `time`. The component renders an alert box with an icon, a label, and a
close button. */
export const Alert = ({ show, type = "success", label = "", autoClose = false, time = 3000 }: AlertProps) => {
    const [innerShow, setInnerShow] = useState(false)


    useEffect(() => {
        if (show) {
            setInnerShow(true);
            autoClose && setTimeout(() => {
                setInnerShow(false);
            }, time)
        }
    }, [show])


    /**
     * The function returns the corresponding icon based on the given type.
     */
    const icon = () => iconLookup[type];

    return (
        // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
        <div className={`p-5 ${classes[type]} w-auto border rounded border-opacity-40 fixed bottom-7 -right-[100%] ${(show && innerShow) && '!right-3'} transition-all duration-1000`} >
            <div className='w-full h-full flex justify-center items-center gap-5'>
                <div>
                    {icon()}
                </div>
                <div>
                    {(show && innerShow) && label}
                </div>
                <AiOutlineClose size='1rem' onClick={() => setInnerShow(false)} />
            </div>
        </div>
    )
}
