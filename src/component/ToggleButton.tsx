import { Colors } from "./Sidebar"
export default function ToggleButton({ isSidebarNotOpen, colors }: { isSidebarNotOpen: boolean, colors: Colors }) {
    const defaultStyle = `h-1 w-4 my-1 rounded-full bg-white transition ease transform duration-300`
    return (
        <div style={{ background: colors.sidebarBg }} className="pt-2 rounded-s-lg" dir='rtl'>
            <div
                className="flex flex-col items-center justify-center w-8 h-8 group"
            >
                <div
                    className={`${defaultStyle} ${isSidebarNotOpen
                        ? "translate-y-[7px] rotate-45 opacity-70 group-hover:opacity-100"
                        : "opacity-70 group-hover:opacity-100"
                        }`}
                />
                <div
                    className={`${defaultStyle} ${isSidebarNotOpen ? "opacity-0" : "opacity-70 group-hover:opacity-100 -translate-y-1"
                        }`}
                />
                <div
                    className={`${defaultStyle} ${isSidebarNotOpen
                        ? "-translate-y-[14.4px] -rotate-45 opacity-70 group-hover:opacity-100"
                        : "opacity-70 group-hover:opacity-100 -translate-y-2"
                        }`}
                />
            </div>
        </div>
    )
}
