import { useState } from "react"
import { FaTrash, FaChevronDown } from 'react-icons/fa'

/**
 * The above type defines the props for an autocomplete component in TypeScript React, including
 * placeholder, value, setValue, and options.
 * @property {string} placeholder - The `placeholder` property is an optional string that specifies the
 * placeholder text to be displayed in the input field of the autocomplete component. It is used to
 * provide a hint or example of the expected input format.
 * @property {string | undefined} value - The `value` property is a string or undefined value that
 * represents the current value of the autocomplete input field.
 * @property {Function} setValue - The `setValue` property is a function that is used to update the
 * value of the autocomplete component. It takes a single argument, which is the new value for the
 * autocomplete.
 * @property {{ label: string, value: any }[]} options - The `options` property is an array of objects
 * that represent the available options for the autocomplete component. Each object in the array has
 * two properties: `label` and `value`. The `label` property represents the display text for the
 * option, while the `value` property represents the corresponding value for
 */
export type AutoCompleteProps = {
  placeholder?: string,
  theme?: {
    [key: string]: { mainColor?: string }
  },
  value: string | undefined,
  setValue: Function,
  options: { label: string, value: any }[]
}

const themeDefault = {
  light: {
    mainColor: "#285fcc"
  },
  dark: {
    mainColor: "#285fcc"
  },
}

/**
 * The AutoComplete component is a TypeScript React component that provides an input field with
 * autocomplete functionality.
 * @param {AutoCompleteProps}  - - `value: userInput` - The current value of the input field.
 * @returns The code is returning a React component called `AutoComplete`.
 */
export const AutoComplete = ({ value: userInput, setValue: setUserInput, options, placeholder = "", theme = themeDefault }: AutoCompleteProps) => {
  const [focus, setFocus] = useState(true)
  const [focusList, setFocusList] = useState(true)
  const [filtered, setFiltered] = useState(options);

  /**
   * The function filters an array of options based on a given value and updates the filtered array.
   * @param {string} value - The `value` parameter is a string that represents the value to filter the
   * options by.
   */
  const filter = (value: string) => {
    const tmp = options.filter(({ label }: { label: string }) => label.toLocaleLowerCase().includes(value.toLocaleLowerCase()))
    setFiltered(tmp)
  }

  return (
    <div className='relative w-full overflow-visible z-50'>
      <input
        value={userInput}
        type='text'
        className={`rounded-md w-full p-3 border-[1px] focus:outline-none border-gray-200 focus:border-[${theme.light.mainColor}]`}
        onFocus={() => setFocus(true)}
        onBlur={() => setFocus(false)}
        onKeyUp={(e: any) => filter(e.target.value)}
        onChange={(e: any) => setUserInput(e.target.value)}
      />
      <div className={`absolute px-3 top-0 bottom-0 flex items-center justify-center transition-all h-full text-gray-400 font-light right-0`} >
        <FaTrash size='0.7rem' className="hover:text-red-500 mx-3" onClick={() => setUserInput("")} />
        <FaChevronDown size='0.7rem' className={`${(focus || focusList) && "rotate-180"} hover:text-[${theme.light.mainColor}] transition-transform`} onClick={() => setFocusList(!focusList)} />
      </div>
      <div style={{ userSelect: "none", WebkitUserSelect: "none", msUserSelect: "none" }} className={`absolute pointer-events-none px-3 top-0 bottom-0 flex items-center justify-center transition-all ${(focus || userInput) ? "text-xs z-10 top-0 h-0 bg-white text-stroke text-gray-400" : "h-full text-gray-400 font-light"} ${focus && `!text-[${theme.light.mainColor}]`}`}>{placeholder}</div>

      {(focus || focusList) && <div className="bg-white absolute z-10 rounded-md px-3 w-full border max-h-[300px] overflow-auto border-gray-200 font-normal text-gray-500" onMouseEnter={() => setFocusList(true)} onMouseLeave={() => setFocusList(false)}>
        {filtered && filtered.map(({ label, value }: { label: string, value: any }) => (
          <div key={value} className="w-full hover:bg-gray-50 my-2" onClick={() => { setUserInput(value); setFocusList(false) }} onFocus={() => setFocusList(true)} onBlur={() => setFocusList(false)}>{label}</div>
        ))}
      </div>}
    </div>
  )
}
